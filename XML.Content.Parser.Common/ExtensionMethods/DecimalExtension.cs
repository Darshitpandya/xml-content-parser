using System;

namespace XML.Content.Parser.Common.ExtensionMethods
{
  /// <summary>
  /// 
  /// </summary>
  public static class DecimalExtension
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static decimal RoundToMoneyValue(this decimal value)
    {
      return Math.Round(value, 2, MidpointRounding.AwayFromZero);
    }
  }
}

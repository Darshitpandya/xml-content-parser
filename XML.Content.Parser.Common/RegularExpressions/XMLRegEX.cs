namespace XML.Content.Parser.Common.RegularExpressions
{
  public class XmlRegEx
  {
    /// <summary>
    /// 
    /// </summary>
    public const string XmlOpenElementRegex = "<(\\w+)>";
    /// <summary>
    /// 
    /// </summary>
    public const string XmlCloseElementRegex = "</(\\w+)>";
    /// <summary>
    /// 
    /// </summary>
    public const string XmlContentRegex = "<{0}>(.*?)</{0}>";
  }
}

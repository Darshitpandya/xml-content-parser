using System;
using System.Runtime.Serialization;

namespace XML.Content.Parser.Common.Exceptions
{
  /// <summary>
  /// Exception for XML content parser.
  /// </summary>
  public class XmlContentParserException : Exception
  {
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    /// <param name="message"></param>
    public XmlContentParserException(string message) : base(message)
    {
    }

    /// <inheritdoc />
    /// <summary>
    /// </summary>
    /// <param name="message"></param>
    /// <param name="innerException"></param>
    public XmlContentParserException(string message, Exception innerException) : base(message, innerException)
    {
    }

    /// <inheritdoc />
    /// <summary>
    /// </summary>
    /// <param name="info"></param>
    /// <param name="context"></param>
    protected XmlContentParserException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
  }
}

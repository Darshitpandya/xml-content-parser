using System;
using System.Collections.Generic;
using XML.Content.Parser.Business.Interfaces;
using XML.Content.Parser.Business.Validators;

namespace XML.Content.Parser.Business.Factories
{
  public class XmlValidationFactory : IXmlValidationFactory
  {
    private readonly IIdentifyXmlElementsService _identifyXmlElementsService;
    private readonly IValidationService _validationService;

    /// <summary>
    /// Initializes a new instance of the <see cref="XmlValidationFactory"/> class.
    /// </summary>
    /// <param name="identifyXmlElementsService">The identify XML elements service.</param>
    /// <param name="validationService">The validation repository.</param>
    /// <exception cref="ArgumentNullException">
    /// identifyXmlElementsService
    /// or
    /// validationRepository
    /// </exception>
    public XmlValidationFactory(IIdentifyXmlElementsService identifyXmlElementsService, IValidationService validationService)
    {
      _identifyXmlElementsService = identifyXmlElementsService ?? throw new ArgumentNullException(nameof(identifyXmlElementsService));
      _validationService = validationService ?? throw new ArgumentNullException(nameof(validationService));
    }

    /// <inheritdoc />
    /// <summary>
    /// Creates the XML validators.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<IXmlElementValidator> CreateValidators()
    {
      return new List<IXmlElementValidator>
      {
        new ContainsXmlElements(_identifyXmlElementsService),
        new NoMissingXmlElements(_identifyXmlElementsService),
        new ValidXmlElements(_identifyXmlElementsService),
        new MandatoryXmlElements(_identifyXmlElementsService, _validationService)
      };
    }
  }
}

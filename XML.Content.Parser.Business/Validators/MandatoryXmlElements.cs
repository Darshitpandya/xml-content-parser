using System;
using System.Collections.Generic;
using System.Linq;
using XML.Content.Parser.Business.Interfaces;
using XML.Content.Parser.Common.Exceptions;
using XML.Content.Parser.Common.RegularExpressions;

namespace XML.Content.Parser.Business.Validators
{
  public class MandatoryXmlElements : IXmlElementValidator
  {
    private readonly IIdentifyXmlElementsService _identifyXmlElementsService;
    private readonly IValidationService _validationService;

    /// <summary>
    /// Initializes a new instance of the <see cref="MandatoryXmlElements"/> class.
    /// </summary>
    /// <param name="identifyXmlElementsService">The identify XML elements service.</param>
    /// <param name="validationService">The validation repository.</param>
    /// <exception cref="ArgumentNullException">
    /// identifyXmlElementsService
    /// or
    /// validationRepository
    /// </exception>
    public MandatoryXmlElements(IIdentifyXmlElementsService identifyXmlElementsService, IValidationService validationService)
    {
      if (identifyXmlElementsService == null) throw new ArgumentNullException(nameof(identifyXmlElementsService));
      if (validationService == null) throw new ArgumentNullException(nameof(validationService));

      _identifyXmlElementsService = identifyXmlElementsService;
      _validationService = validationService;
    }

    /// <inheritdoc />
    /// <summary>
    /// Validates the specified message content.
    /// Ensures that the <see cref="!:messageContent" /> contains all manadtory XML elements.
    /// </summary>
    /// <param name="messageContent">Content of the message.</param>
    /// <exception cref="T:System.ArgumentException">Value cannot be null or whitespace. - messageContent</exception>
    /// <exception cref="T:XML.Content.Parser.Common.Exceptions.XmlContentParserException"></exception>
    public void Validate(string messageContent)
    {
      if (string.IsNullOrWhiteSpace(messageContent))
        throw new ArgumentException("Value cannot be null or whitespace.", nameof(messageContent));

      IEnumerable<string> mandatoryXmlElements = _validationService.GetMandatoryXmlElements().ToList();

      var isValid = mandatoryXmlElements.All(xmlElement =>
      {
        var xmlContent = _identifyXmlElementsService.ExtractXmlContent(messageContent, XmlRegEx.XmlContentRegex, xmlElement);
        return !string.IsNullOrWhiteSpace(xmlContent) && !xmlContent.Equals($"{xmlElement}{xmlElement.Insert(1, "/")}");
      });

      if (!isValid)
      {
        throw new XmlContentParserException($"The specified message content does not contain all mandatory XML elements. Mandatory elements: '{string.Join(",", mandatoryXmlElements)}'.");
      }
    }
  }
}

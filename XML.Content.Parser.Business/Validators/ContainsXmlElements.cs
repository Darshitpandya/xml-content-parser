using System;
using System.Linq;
using XML.Content.Parser.Business.Interfaces;
using XML.Content.Parser.Common.Exceptions;
using XML.Content.Parser.Common.RegularExpressions;


namespace XML.Content.Parser.Business.Validators
{
  public class ContainsXmlElements : IXmlElementValidator
  {
    private readonly IIdentifyXmlElementsService _identifyXmlElementsService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ContainsXmlElements"/> class.
    /// </summary>
    /// <param name="identifyXmlElementsService">The identify XML elements service.</param>
    /// <exception cref="ArgumentNullException">identifyXmlElementsService</exception>
    public ContainsXmlElements(IIdentifyXmlElementsService identifyXmlElementsService)
    {
      _identifyXmlElementsService = identifyXmlElementsService ?? throw new ArgumentNullException(nameof(identifyXmlElementsService));
    }

    /// <inheritdoc />
    /// <summary>
    /// Validates the specified message content.
    /// Ensures that the <see cref="!:messageContent" /> contains an opening and closing XML element.
    /// </summary>
    /// <param name="messageContent">Content of the message.</param>
    /// <exception cref="T:System.ArgumentException">Value cannot be null or whitespace. - messageContent</exception>
    /// <exception cref="T:XML.Content.Parser.Common.Exceptions.XmlContentParserException">The specified message content does not contain any valid XML elements.</exception>
    public void Validate(string messageContent)
    {
      if (string.IsNullOrWhiteSpace(messageContent))
        throw new ArgumentException("Value cannot be null or whitespace.", nameof(messageContent));

      var isValid = HasXmlElements(messageContent, XmlRegEx.XmlOpenElementRegex) &&
                     HasXmlElements(messageContent, XmlRegEx.XmlCloseElementRegex);

      if (!isValid)
      {
        throw new XmlContentParserException("The specified message content does not contain any valid XML elements.");
      }
    }

    private bool HasXmlElements(string messageContent, string regex)
    {
      return _identifyXmlElementsService.IdentifyXmlElements(messageContent, regex).Any();
    }
  }
}

using System;
using System.Collections.Generic;
using System.Linq;
using XML.Content.Parser.Business.Interfaces;
using XML.Content.Parser.Common.Exceptions;
using XML.Content.Parser.Common.RegularExpressions;

namespace XML.Content.Parser.Business.Validators
{
  public class NoMissingXmlElements : IXmlElementValidator
  {
    private readonly IIdentifyXmlElementsService _identifyXmlElementsService;

    /// <summary>
    /// Initializes a new instance of the <see cref="NoMissingXmlElements"/> class.
    /// </summary>
    /// <param name="identifyXmlElementsService">The identify XML elements service.</param>
    /// <exception cref="ArgumentNullException">identifyXmlElementsService</exception>
    public NoMissingXmlElements(IIdentifyXmlElementsService identifyXmlElementsService)
    {
      _identifyXmlElementsService = identifyXmlElementsService ?? throw new ArgumentNullException(nameof(identifyXmlElementsService));
    }

    /// <inheritdoc />
    /// <summary>
    /// Validates the specified message content.
    /// Ensures that the <see cref="!:messageContent" /> contains for all XML elements a corresponding opening and closing XML element pair.
    /// </summary>
    /// <param name="messageContent">Content of the message.</param>
    /// <exception cref="T:System.ArgumentException">Value cannot be null or whitespace. - messageContent</exception>
    /// <exception cref="T:XML.Content.Parser.Common.Exceptions.XmlContentParserException">The specified message content contains XML elements without it's corresponding pair.</exception>
    public void Validate(string messageContent)
    {
      if (string.IsNullOrWhiteSpace(messageContent))
        throw new ArgumentException("Value cannot be null or whitespace.", nameof(messageContent));

      var missingXmlElements = new List<string>();

      var openingXmlElements = IdentifyXmlElements(messageContent, XmlRegEx.XmlOpenElementRegex);
      var closingXmlElements = IdentifyXmlElements(messageContent, XmlRegEx.XmlCloseElementRegex);

      CheckForMissingXmlElements(openingXmlElements, closingXmlElements, missingXmlElements);
      CheckForMissingXmlElements(closingXmlElements, openingXmlElements, missingXmlElements);
      if (missingXmlElements.Any())
      {
        throw new XmlContentParserException("The specified message content contains XML elements without it's corresponding pair.");
      }
    }

    private static void CheckForMissingXmlElements(Dictionary<string, int> xmlElements1, Dictionary<string, int> xmlElements2, List<string> missingXmlElements)
    {
      foreach (var xmlElement in xmlElements1)
      {
        var opposingXmlElement = TransformToOpposingXmlElement(xmlElement.Key);

        var containsOpposingXmlElementKey = xmlElements2.ContainsKey(opposingXmlElement);
        if (!containsOpposingXmlElementKey)
        {
          missingXmlElements.Add(xmlElement.Key);
          continue;
        }

        var matchingElementCount = xmlElements2[opposingXmlElement] == xmlElement.Value;
        if (!matchingElementCount)
        {
          missingXmlElements.Add(xmlElement.Key);
        }
      }
    }

    private Dictionary<string, int> IdentifyXmlElements(string messageContent, string regex)
    {
      var identifyXmlMatches = _identifyXmlElementsService.IdentifyXmlElements(messageContent, regex);

      return identifyXmlMatches.GroupBy(xmlElement => xmlElement)
          .ToDictionary(
              xmlElement => xmlElement.Key,
              xmlElement => xmlElement.Count());
    }

    private static string TransformToOpposingXmlElement(string xmlElement)
    {
      var isClosingXmlElement = xmlElement.IndexOf("/", StringComparison.InvariantCultureIgnoreCase) == 1;

      return isClosingXmlElement ? xmlElement.Remove(1, 1) : xmlElement.Insert(1, "/");
    }
  }
}

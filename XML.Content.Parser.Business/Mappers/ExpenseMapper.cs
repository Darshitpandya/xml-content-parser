using System;
using XML.Content.Parser.Business.Contracts;
using XML.Content.Parser.Business.Domain;

namespace XML.Content.Parser.Business.Mappers
{
  public static class ExpenseMapper
  {
    /// <summary>
    /// Initializes an <see cref="Expense"/> <see cref="object"/>.
    /// </summary>
    /// <param name="expense">The expense dto.</param>
    /// <param name="vendor">The vendor dto.</param>
    /// <param name="description">The description dto.</param>
    /// <param name="eventDate">The event date dto.</param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException">expenseDto</exception>
    public static Expense Map(ExpenseContract expense, VendorContract vendor, DescriptionContract description, EventDateContract eventDate)
    {
      if (expense == null) throw new ArgumentNullException(nameof(expense));

      return new Expense(
        expense.Expense.CostCentre ?? string.Empty,
        expense.Expense.Total,
        vendor?.Vendor ?? string.Empty,
        description?.Description ?? string.Empty,
        eventDate?.Date ?? string.Empty);
    }
  }
}

using Newtonsoft.Json;
using XML.Content.Parser.Business.Constants;

namespace XML.Content.Parser.Business.Contracts
{
  public class VendorContract
  {
    /// <summary>
    /// Gets or sets the vendor.
    /// </summary>
    /// <value>
    /// The vendor.
    /// </value>
    [JsonProperty(ExpenseConstants.Vendor)]
    public string Vendor { get; set; }
  }
}

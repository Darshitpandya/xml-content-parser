using Newtonsoft.Json;
using XML.Content.Parser.Business.Constants;

namespace XML.Content.Parser.Business.Contracts
{
  public class ExpenseContract
  { /// <summary>
    /// Gets or sets the expense.
    /// </summary>
    /// <value>
    /// The expense.
    /// </value>
    [JsonProperty(ExpenseConstants.Expense)]
    public ExpenseDataContract Expense { get; set; }
  }
}

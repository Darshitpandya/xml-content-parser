using Newtonsoft.Json;
using XML.Content.Parser.Business.Constants;

namespace XML.Content.Parser.Business.Contracts
{
  public class DescriptionContract
  {
    /// <summary>
    /// Gets or sets the description.
    /// </summary>
    /// <value>
    /// The description.
    /// </value>
    [JsonProperty(ExpenseConstants.Description)]
    public string Description { get; set; }
  }
}

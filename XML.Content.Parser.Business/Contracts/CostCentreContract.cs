using Newtonsoft.Json;
using XML.Content.Parser.Business.Constants;

namespace XML.Content.Parser.Business.Contracts
{
 public class CostCentreContract
  {
    /// <summary>
    /// Gets or sets the cost centre.
    /// </summary>
    /// <value>
    /// The cost centre.
    /// </value>
    [JsonProperty(ExpenseConstants.CostCentre)]
    public string CostCentre { get; set; }
  }
}

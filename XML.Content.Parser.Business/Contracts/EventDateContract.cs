using Newtonsoft.Json;
using XML.Content.Parser.Business.Constants;

namespace XML.Content.Parser.Business.Contracts
{
  public class EventDateContract
  {
    /// <summary>
    /// Gets or sets the date.
    /// </summary>
    /// <value>
    /// The date.
    /// </value>
    [JsonProperty(ExpenseConstants.Date)]
    public string Date { get; set; }
  }
}

using Newtonsoft.Json;
using XML.Content.Parser.Business.Constants;

namespace XML.Content.Parser.Business.Contracts
{
  public class PaymentMethodContract
  {
    /// <summary>
    /// Gets or sets the payment method.
    /// </summary>
    /// <value>
    /// The payment method.
    /// </value>
    [JsonProperty(ExpenseConstants.PaymentMethod)]
    public string PaymentMethod { get; set; }
  }
}

using Newtonsoft.Json;
using XML.Content.Parser.Business.Constants;

namespace XML.Content.Parser.Business.Contracts
{
  public class TotalContract
  {
    /// <summary>
    /// Gets or sets the total.
    /// </summary>
    /// <value>
    /// The total.
    /// </value>
    [JsonProperty(ExpenseConstants.Total)]
    public decimal Total { get; set; }
  }
}

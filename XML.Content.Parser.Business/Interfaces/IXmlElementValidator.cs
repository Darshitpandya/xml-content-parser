namespace XML.Content.Parser.Business.Interfaces
{
  public interface IXmlElementValidator
  {
    /// <summary>
    /// Validates the specified message content.
    /// </summary>
    /// <param name="messageContent">Content of the message.</param>
    void Validate(string messageContent);
  }
}

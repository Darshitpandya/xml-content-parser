namespace XML.Content.Parser.Business.Interfaces
{
  public interface IXmlDeserializerService
  {
    /// <summary>
    /// Deserializes the specified message content.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="messageContent">Content of the message.</param>
    /// <returns></returns>
    T Deserialize<T>(string messageContent);
  }
}

using System.Collections.Generic;

namespace XML.Content.Parser.Business.Interfaces
{
  public interface IXmlValidationFactory
  {
    /// <summary>
    /// Creates the XML validators.
    /// </summary>
    /// <returns></returns>
    IEnumerable<IXmlElementValidator> CreateValidators();
  }
}

using System.Collections.Generic;

namespace XML.Content.Parser.Business.Interfaces
{
  public interface IValidationService
  {
    /// <summary>
    /// Gets the mandatory XML elements.
    /// </summary>
    /// <returns></returns>
    IEnumerable<string> GetMandatoryXmlElements();
  }
}

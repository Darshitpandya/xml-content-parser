using XML.Content.Parser.Business.Domain;

namespace XML.Content.Parser.Business.Interfaces
{
  public interface IExpenseService
  { /// <summary>
    /// Extracts the specified message content.
    /// </summary>
    /// <param name="messageContent">Content of the message.</param>
    /// <returns></returns>
    Expense Extract(string messageContent);
  }
}

using System;
using XML.Content.Parser.Business.Constants;
using XML.Content.Parser.Business.Contracts;
using XML.Content.Parser.Business.Interfaces;
using XML.Content.Parser.Business.Mappers;
using XML.Content.Parser.Common.RegularExpressions;
using Expense = XML.Content.Parser.Business.Domain.Expense;

namespace XML.Content.Parser.Business.Services
{
  public class ExpenseService : IExpenseService
  {
    private readonly IXmlValidationFactory _xmlValidationFactory;
    private readonly IIdentifyXmlElementsService _identifyXmlElementsService;
    private readonly IXmlDeserializerService _xmlDeserializerService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ExpenseService"/> class.
    /// </summary>
    /// <param name="xmlValidationFactory">The XML validation factory.</param>
    /// <param name="identifyXmlElementsService">The identify XML elements service.</param>
    /// <param name="xmlDeserializerService">The XML deserializer service.</param>
    /// <exception cref="ArgumentNullException">
    /// xmlValidationFactory
    /// or
    /// identifyXmlElementsService
    /// or
    /// xmlDeserializerService
    /// </exception>
    public ExpenseService(IXmlValidationFactory xmlValidationFactory, IIdentifyXmlElementsService identifyXmlElementsService,
        IXmlDeserializerService xmlDeserializerService)
    {
      _xmlValidationFactory = xmlValidationFactory ?? throw new ArgumentNullException(nameof(xmlValidationFactory));
      _identifyXmlElementsService = identifyXmlElementsService ?? throw new ArgumentNullException(nameof(identifyXmlElementsService));
      _xmlDeserializerService = xmlDeserializerService ?? throw new ArgumentNullException(nameof(xmlDeserializerService));
    }

    /// <inheritdoc />
    /// <summary>
    /// Extracts the specified message content.
    /// </summary>
    /// <param name="messageContent">Content of the message.</param>
    /// <returns></returns>
    /// <exception cref="T:System.ArgumentException">Value cannot be null or whitespace. - messageContent</exception>
    public Expense Extract(string messageContent)
    {
      if (string.IsNullOrWhiteSpace(messageContent))
        throw new ArgumentException("Value cannot be null or whitespace.", nameof(messageContent));

      ValidateMessageContent(messageContent);

      var expense = ExtractAndDeserializeExpenseXmlElement(messageContent);
      var vendor = ExtractAndDeserializeXmlElement<VendorContract>(messageContent, ExpenseConstants.Vendor);
      var description = ExtractAndDeserializeXmlElement<DescriptionContract>(messageContent, ExpenseConstants.Description);
      var eventDate = ExtractAndDeserializeXmlElement<EventDateContract>(messageContent, ExpenseConstants.Date);

      return ExpenseMapper.Map(expense, vendor, description, eventDate);
    }

    private void ValidateMessageContent(string messageContent)
    {
      var xmlElementValidators = _xmlValidationFactory.CreateValidators();

      foreach (var xmlElementValidator in xmlElementValidators)
      {
        xmlElementValidator.Validate(messageContent);
      }
    }

    private ExpenseContract ExtractAndDeserializeExpenseXmlElement(string messageContent)
    {
      var expenseContract = ExtractAndDeserializeXmlElement<ExpenseContract>(messageContent, ExpenseConstants.Expense);
      if (expenseContract != null)
      {
        return expenseContract;
      }

      var costCentre = ExtractAndDeserializeXmlElement<CostCentreContract>(messageContent, ExpenseConstants.CostCentre);
      var total = ExtractAndDeserializeXmlElement<TotalContract>(messageContent, ExpenseConstants.Total);
      var paymentMethod = ExtractAndDeserializeXmlElement<PaymentMethodContract>(messageContent, ExpenseConstants.PaymentMethod);

      return new ExpenseContract
      {
        Expense = new ExpenseDataContract
        {
          CostCentre = costCentre?.CostCentre,
          Total = total.Total,
          PaymentMethod = paymentMethod?.PaymentMethod
        }
      };
    }

    private T ExtractAndDeserializeXmlElement<T>(string messageContent, string element)
    {
      var xmlContent = _identifyXmlElementsService.ExtractXmlContent(messageContent, XmlRegEx.XmlContentRegex, element);

      return !string.IsNullOrWhiteSpace(xmlContent) ? _xmlDeserializerService.Deserialize<T>(xmlContent) : default(T);
    }
  }
}

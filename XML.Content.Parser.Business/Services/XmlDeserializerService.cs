using System;
using System.Xml;
using Newtonsoft.Json;
using XML.Content.Parser.Business.Interfaces;
using XML.Content.Parser.Common.Exceptions;

namespace XML.Content.Parser.Business.Services
{
  public class XmlDeserializerService : IXmlDeserializerService
  {
    /// <inheritdoc />
    /// <summary>
    /// Deserializes the specified message content.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="messageContent">Content of the message.</param>
    /// <returns></returns>
    /// <exception cref="T:System.ArgumentException">Value cannot be null or whitespace. - messageContent</exception>
    /// <exception cref="T:XML.Content.Parser.Common.Exceptions.XmlContentParserException"></exception>
    public T Deserialize<T>(string messageContent)
    {
      if (string.IsNullOrWhiteSpace(messageContent))
        throw new ArgumentException("Value cannot be null or whitespace.", nameof(messageContent));

      try
      {
        var xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(messageContent);

        return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeXmlNode(xmlDocument));
      }
      catch (Exception exception)
      {
        throw new XmlContentParserException($"The specified message content could not be deserialized into type: '{typeof(T).Name}'.", exception);
      }
    }
  }
}

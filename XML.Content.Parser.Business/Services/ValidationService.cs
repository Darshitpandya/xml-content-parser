using System.Collections.Generic;
using XML.Content.Parser.Business.Interfaces;

namespace XML.Content.Parser.Business.Services
{
  public class ValidationService : IValidationService
  {
    /// <inheritdoc />
    /// <summary>
    /// Gets the mandatory XML elements.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<string> GetMandatoryXmlElements()
    {
      return new List<string>
      {
        "<total>"
      };
    }
  }
}

using NSubstitute;
using XML.Content.Parser.Business.Factories;
using XML.Content.Parser.Business.Interfaces;
using XML.Content.Parser.Business.Services;
using XML.Content.Parser.Business.Validators;

namespace XML.Content.Parser.Business.Tests.Stubs.TestFoundations
{
  public class TestFoundation
  {
    protected TestFoundation()
    {
      ValidationService = Substitute.For<IValidationService>();
      IdentifyXmlElementsService = new IdentifyXmlElementsService();
      ContainsXmlElementsValidator = new ContainsXmlElements(IdentifyXmlElementsService);
      NoMissingXmlElementsValidator = new NoMissingXmlElements(IdentifyXmlElementsService);
      ValidXmlElementsValidator = new ValidXmlElements(IdentifyXmlElementsService);
      MandatoryXmlElementsValidator = new MandatoryXmlElements(IdentifyXmlElementsService, ValidationService);
      XmlValidationFactory = new XmlValidationFactory(IdentifyXmlElementsService, ValidationService);
      XmlDeserializerService = new XmlDeserializerService();
      ExpenseService = new ExpenseService(XmlValidationFactory, IdentifyXmlElementsService, XmlDeserializerService);
    }

    // Services
    protected IIdentifyXmlElementsService IdentifyXmlElementsService { get; }

    protected IXmlDeserializerService XmlDeserializerService { get; }

    protected IExpenseService ExpenseService { get; }

    protected IValidationService ValidationService { get; }

    // Validators
    protected IXmlElementValidator ContainsXmlElementsValidator { get; }

    protected IXmlElementValidator NoMissingXmlElementsValidator { get; }

    protected IXmlElementValidator ValidXmlElementsValidator { get; }

    protected IXmlElementValidator MandatoryXmlElementsValidator { get; }

    // Factories
    protected IXmlValidationFactory XmlValidationFactory { get; }
  }
}

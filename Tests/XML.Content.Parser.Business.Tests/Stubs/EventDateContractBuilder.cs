using XML.Content.Parser.Business.Contracts;

namespace XML.Content.Parser.Business.Tests.Stubs
{
  public class EventDateContractBuilder
  {
    private readonly string _date;

    public EventDateContractBuilder()
    {
      _date = "Tuesday 27 April 2017";
    }

    public EventDateContract Build()
    {
      return new EventDateContract
      {
        Date = _date
      };
    }
  }
}

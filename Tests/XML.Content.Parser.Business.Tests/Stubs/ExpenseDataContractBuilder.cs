using XML.Content.Parser.Business.Contracts;

namespace XML.Content.Parser.Business.Tests.Stubs
{
 public class ExpenseDataContractBuilder
  {
    private string _costCentre;
    private decimal _total;

    public ExpenseDataContractBuilder()
    {
      _costCentre = "DEV002";
      _total = 1024.01m;
    }

    public ExpenseDataContract Build()
    {
      return new ExpenseDataContract
      {
        CostCentre = _costCentre,
        Total = _total
      };
    }

    public ExpenseDataContractBuilder WithCostCentre(string costCentre)
    {
      _costCentre = costCentre;
      return this;
    }

    public ExpenseDataContractBuilder WithTotal(decimal total)
    {
      _total = total;
      return this;
    }
  }
}

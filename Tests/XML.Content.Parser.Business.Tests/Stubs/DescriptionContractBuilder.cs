using XML.Content.Parser.Business.Contracts;

namespace XML.Content.Parser.Business.Tests.Stubs
{
  public class DescriptionContractBuilder
  {
    private readonly string _description;

    public DescriptionContractBuilder()
    {
      _description = "development team's project end celebration dinner";
    }

    public DescriptionContract Build()
    {
      return new DescriptionContract
      {
        Description = _description
      };
    }
  }
}

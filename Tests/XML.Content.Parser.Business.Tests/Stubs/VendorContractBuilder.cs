using XML.Content.Parser.Business.Contracts;

namespace XML.Content.Parser.Business.Tests.Stubs
{
  public class VendorContractBuilder
  {
    private readonly string _vendor;

    public VendorContractBuilder()
    {
      _vendor = "Viaduct Steakhouse";
    }

    public VendorContract Build()
    {
      return new VendorContract
      {
        Vendor = _vendor
      };
    }
  }
}

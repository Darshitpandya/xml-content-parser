using NSubstitute;
using System.Collections.Generic;
using System.ComponentModel;
using XML.Content.Parser.Business.Tests.Stubs.TestFoundations;
using XML.Content.Parser.Common.Exceptions;
using Xunit;

namespace XML.Content.Parser.Business.Tests.Services
{
  [Category("XmlExtraction")]
  public class ExpenseServiceTests : TestFoundation
  {
    internal void Setup()
    {
      ValidationService.GetMandatoryXmlElements().Returns(new List<string>
            {
                "<total>"
            });
    }

    [Theory]
    [InlineData(@"Hi Yvaine,Please create an expense claim for the below. Relevant details are marked up as
requested...<expense><cost_centre>DEV002</cost_centre><total>1024.01</total><payment_method>personal card</payment_method>
</expense>From: Ivan Castle Sent: Friday, 16 February 2018 10:32 AM To: Antoine Lloyd <Antoine.Lloyd@example.com>
Subject: test Hi Antoine,
Please create a reservation at the <vendor>Viaduct Steakhouse</vendor> our
<description>development team’s project end celebration dinner</description> on
<date>Tuesday 27 April 2017</date>. We expect to arrive around
7.15pm. Approximately 12 people but I’ll confirm exact numbers closer to the day.
Regards,
Ivan")]
    public void When_Passing_MessageContent_Then_Extract_It_And_Should_Return_Expected_Data(string messageContent)
    {
      //Act
      var expense = ExpenseService.Extract(messageContent);

      //Assert
      Assert.Equal("DEV002", expense.CostCentre);
      Assert.Equal(1024.01m, expense.TotalInclGst);
      Assert.Equal(890.44m, expense.TotalExclGst);
      Assert.Equal(133.57m, expense.GstAmount);
      Assert.Equal("Viaduct Steakhouse", expense.Vendor);
      Assert.Equal("development team’s project end celebration dinner", expense.Description);
      Assert.Equal("Tuesday 27 April 2017", expense.EventDate);
    }

    [Theory]
    [InlineData(@"<expense>
    <total>1024.01</total><payment_method>personal card</payment_method>
</expense>")]
    public void When_Passing_MessageContent_Without_CostCentre_Then_Extract_It_And_Should_Return_UNKNOWN_CostCentre_And_Expected_Data(string messageContent)
    {
      //Act
      var expense = ExpenseService.Extract(messageContent);

      //Assert
      Assert.Equal("UNKNOWN", expense.CostCentre);
      Assert.Equal(1024.01m, expense.TotalInclGst);
      Assert.Equal(890.44m, expense.TotalExclGst);
      Assert.Equal(133.57m, expense.GstAmount);
      Assert.Equal(string.Empty, expense.Vendor);
      Assert.Equal(string.Empty, expense.Description);
      Assert.Equal(string.Empty, expense.EventDate);
    }

    [Theory]
    [InlineData(@"<cost_centre>DEV002</cost_centre>
<total>1024.01</total>
<payment_method>personal card</payment_method>
<vendor>Viaduct Steakhouse</vendor>
<description>development team’s project end celebration dinner</description>
<date>Tuesday 27 April 2017</date>")]
    public void When_Passing_MessageContent_Without_ParentBlock_Then_Extract_It_And_Should_Return_Expected_Data(string messageContent)
    {

      //Act
      Business.Domain.Expense expense = ExpenseService.Extract(messageContent);

      //Assert
      Assert.Equal("DEV002", expense.CostCentre);
      Assert.Equal(1024.01m, expense.TotalInclGst);
      Assert.Equal(890.44m, expense.TotalExclGst);
      Assert.Equal(133.57m, expense.GstAmount);
      Assert.Equal("Viaduct Steakhouse", expense.Vendor);
      Assert.Equal("development team’s project end celebration dinner", expense.Description);
      Assert.Equal("Tuesday 27 April 2017", expense.EventDate);
    }

    [Theory]
    [InlineData(@"<expense><cost_centre>DEV002</cost_centre><payment_method>personal card</payment_method></expense>")]
    public void When_Passing_MessageContent_Without_Total_Then_Extract_It_And_Should_Throw_Exception(string messageContent)
    {
      //Arrange
      Setup();

      //Act
      var exception = Assert.Throws<XmlContentParserException>(() =>
        {
          ExpenseService.Extract(messageContent);
        });

      //Assert
      Assert.Equal("The specified message content does not contain all mandatory XML elements. Mandatory elements: '<total>'.", exception.Message);
    }

    [Theory]
    [InlineData(@"<email>
    <from>Ivan Castle</from>
    <sent>Friday, 16 February 2018 10:32 AM</sent>
    <to>Antoine Lloyd <Antoine.Lloyd@example.com></to>
    <subject>test</subject>
</email>")]
    public void When_Passing_MessageContent_Without_NonUseCaseXml_Then_Extract_It_And_Should_Throw_Exception(string messageContent)
    {
      //Arrange
      Setup();

      //Act
      XmlContentParserException exception = Assert.Throws<XmlContentParserException>(() =>
      {
        ExpenseService.Extract(messageContent);
      });

      //Assert
      Assert.Equal("The specified message content does not contain all mandatory XML elements. Mandatory elements: '<total>'.", exception.Message);
    }

    [Theory]
    [InlineData("Content without XML")]
    public void When_Passing_MessageContent_Without_No_XML_Element_Then_Extract_It_And_Should_Throw_Exception(string messageContent)
    {
      //Act
      XmlContentParserException exception = Assert.Throws<XmlContentParserException>(() =>
        {
          ExpenseService.Extract(messageContent);
        });

      //Assert
      Assert.Equal("The specified message content does not contain any valid XML elements.", exception.Message);
    }

    [Theory]
    [InlineData(@"<expense><cost_centre>DEV002</cost_centre><total>1024.01</total><payment_method>personal card</payment_method>")]
    public void When_Passing_MessageContent_Without_Clsoing_XML_Element_Then_Extract_It_And_Should_Throw_Exception(string messageContent)
    {
      //Act
      XmlContentParserException exception = Assert.Throws<XmlContentParserException>(() =>
        {
          ExpenseService.Extract(messageContent);
        });

      //Assert
      Assert.Equal("The specified message content contains XML elements without it's corresponding pair.", exception.Message);
    }
  }
}

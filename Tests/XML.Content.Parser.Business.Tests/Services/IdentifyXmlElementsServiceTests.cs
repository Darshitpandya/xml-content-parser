using System.Linq;
using XML.Content.Parser.Business.Tests.Stubs.TestFoundations;
using XML.Content.Parser.Common.RegularExpressions;
using Xunit;

namespace XML.Content.Parser.Business.Tests.Services
{
  public class IdentifyXmlElementsServiceTests : TestFoundation
  {
    [Theory]
    [InlineData("<test1><test2>")]
    [InlineData("<TEST1><TEST2>")]
    public void When_Passing_MessageContent_Then_Identify_Opening_Elements_Should_Return_Opening_Elements(string messageContent)
    {
      //Act
      var xmlElements = IdentifyXmlElementsService.IdentifyXmlElements(messageContent, XmlRegEx.XmlOpenElementRegex).ToList();

      //Assert
      Assert.Contains("<test1>", xmlElements);
      Assert.Contains("<test2>", xmlElements);
    }

    [Theory]
    [InlineData("</test1></test2>")]
    [InlineData("</TEST1></TEST2>")]
    public void When_Passing_MessageContent_Then_Identify_Closing_Elements_Should_Return_Closing_Elements(string messageContent)
    {
      //Act
      var xmlElements = IdentifyXmlElementsService.IdentifyXmlElements(messageContent, XmlRegEx.XmlCloseElementRegex).ToList();

      //Assert
      Assert.Contains("</test1>", xmlElements);
      Assert.Contains("</test2>", xmlElements);
    }

    [Theory]
    [InlineData("hello world")]
    public void When_Passing_MessageContent_Without_XML_Then_Identify_And_Should_Return_Empty_Elements(string messageContent)
    {

      //Act
      var xmlOpeningElements = IdentifyXmlElementsService.IdentifyXmlElements(messageContent, XmlRegEx.XmlOpenElementRegex).ToList();
      var xmlClosingElements = IdentifyXmlElementsService.IdentifyXmlElements(messageContent, XmlRegEx.XmlCloseElementRegex).ToList();

      //Assert
      Assert.Equal(Enumerable.Empty<string>(), xmlOpeningElements);
      Assert.Equal(Enumerable.Empty<string>(), xmlClosingElements);
    }

    [Theory]
    [InlineData("<hello@world.com>")]
    public void When_Passing_MessageContent_With_Symbol_Then_Identify_And_Should_Return_Empty_Elements(string messageContent)
    {

      //Act
      var xmlOpeningElements = IdentifyXmlElementsService.IdentifyXmlElements(messageContent, XmlRegEx.XmlOpenElementRegex).ToList();
      var xmlClosingElements = IdentifyXmlElementsService.IdentifyXmlElements(messageContent, XmlRegEx.XmlCloseElementRegex).ToList();

      //Assert
      Assert.Equal(Enumerable.Empty<string>(), xmlOpeningElements);
      Assert.Equal(Enumerable.Empty<string>(), xmlClosingElements);
    }

    [Theory]
    [InlineData("<test/>")]
    [InlineData("<test />")]
    public void When_Passing_MessageContent_With_Self_Closing_Tag_Then_Identify_And_Should_Return_Empty_Elements(string messageContent)
    {
      //Act
      var xmlElements = IdentifyXmlElementsService.IdentifyXmlElements(messageContent, XmlRegEx.XmlOpenElementRegex).ToList();

      //Assert
      Assert.Equal(Enumerable.Empty<string>(), xmlElements);
    }

    [Theory]
    [InlineData(
@"<expense><cost_centre></cost_centre>
    <total>1024.01</total><payment_method>personal card</payment_method>
</expense>", "expense")]
    [InlineData("<vendor>Viaduct Steakhouse</vendor>", "vendor")]
    public void When_Passing_MessageContent_Then_Extract_And_Should_Return_Extracted_Content(string messageContent, string xmlElement)
    {
      //Act
      var extractXmlContent = IdentifyXmlElementsService.ExtractXmlContent(messageContent, XmlRegEx.XmlContentRegex, xmlElement);

      //Assert
      Assert.Equal(messageContent, extractXmlContent);
    }

    [Theory]
    [InlineData(
@"<expense><cost_centre></cost_centre>
    <total>1024.01</total><payment_method>personal card</payment_method>
</expense>", "test")]
    [InlineData("<vendor>Viaduct Steakhouse</vendor>", "test")]
    public void When_Passing_MessageContent_Then__Unable_To_Extract_And_Should_Return_empty_Content(string messageContent, string xmlElement)
    {
      //Act
      var extractXmlContent = IdentifyXmlElementsService.ExtractXmlContent(messageContent, XmlRegEx.XmlContentRegex, xmlElement);

      //Assert
      Assert.Equal(string.Empty, extractXmlContent);
    }
  }
}

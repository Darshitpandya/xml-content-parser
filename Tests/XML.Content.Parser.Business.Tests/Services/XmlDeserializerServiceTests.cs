using XML.Content.Parser.Business.Tests.Stubs.TestFoundations;
using XML.Content.Parser.Common.Exceptions;
using Xunit;

namespace XML.Content.Parser.Business.Tests.Services
{
  public class XmlDeserializerServiceTests : TestFoundation
  {
    [Theory]
    [InlineData("<child>test me</child>")]
    public void When_Passing_MessageContent_Then_Deserialize_And_Should_Return_Deserialized_Child(string messageContent)
    {
      //Act
      var xmlDeserializationElementTestObject =
        XmlDeserializerService.Deserialize<XmlDeserializationChildElementTestObject>(messageContent);

      //Assert
      Assert.Equal("test me", xmlDeserializationElementTestObject.Child);
    }

    [Theory]
    [InlineData("<parent><child>test us</child></parent>")]
    public void When_Passing_MessageContent_Then_Deserialize_And_Should_Return_Deserialized_Parent_Child(string messageContent)
    {
      //Act
      var xmlDeserializationParentNodeTestObject =
        XmlDeserializerService.Deserialize<XmlDeserializationParentNodeTestObject>(messageContent);

      //Assert
      Assert.Equal("test us", xmlDeserializationParentNodeTestObject.Parent.Child);
    }

    [Theory]
    [InlineData("<test>hello</test>")]
    public void When_Passing_MessageContent_Then_Validate_It_With_Schema_And_Should_Return_Null_Property(string messageContent)
    {
      //Act
      var xmlDeserializationChildElementTestObject =
        XmlDeserializerService.Deserialize<XmlDeserializationChildElementTestObject>(messageContent);

      //Assert
      Assert.Null(xmlDeserializationChildElementTestObject.Child);
    }

    [Theory]
    [InlineData("<test1>hello</test2>")]
    public void When_Passing_MessageContent_Then_Validate_It_With_Invalid_Schema_Should_Throw_Exception(string messageContent)
    {
      //Act
      var exception = Assert.Throws<XmlContentParserException>(() =>
      {
        XmlDeserializerService.Deserialize<XmlDeserializationParentNodeTestObject>(messageContent);
      });

      //Assert
      Assert.Equal("The specified message content could not be deserialized into type: 'XmlDeserializationParentNodeTestObject'.", exception.Message);
    }

    private class XmlDeserializationParentNodeTestObject
    {
      public XmlDeserializationChildElementTestObject Parent { get; set; }
    }

    private class XmlDeserializationChildElementTestObject
    {
      public string Child { get; set; }
    }
  }
}

using System.Collections.Generic;
using System.Linq;
using XML.Content.Parser.Business.Interfaces;
using XML.Content.Parser.Business.Tests.Stubs.TestFoundations;
using XML.Content.Parser.Business.Validators;
using Xunit;

namespace XML.Content.Parser.Business.Tests.Factories
{
  public class XmlValidationFactoryTests : TestFoundation
  {
    [Fact]
    public void When_Validating_Xml_Then_Validate_And_FactoryValidation_Should_Return_Validation_In_Correct_Order()
    {
      //Act
      IEnumerable<IXmlElementValidator> xmlElementValidators = XmlValidationFactory.CreateValidators().ToList();

      //Assert
      Assert.Equal(4,xmlElementValidators.Count());
      Assert.IsType<ContainsXmlElements>(xmlElementValidators.ElementAt(0));
      Assert.IsType<NoMissingXmlElements>(xmlElementValidators.ElementAt(1));
      Assert.IsType<ValidXmlElements>(xmlElementValidators.ElementAt(2));
      Assert.IsType<MandatoryXmlElements>(xmlElementValidators.ElementAt(3));
    }
  }
}

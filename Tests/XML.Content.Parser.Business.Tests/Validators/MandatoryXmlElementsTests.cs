using System.Collections.Generic;
using System.ComponentModel;
using NSubstitute;
using XML.Content.Parser.Business.Tests.Stubs.TestFoundations;
using XML.Content.Parser.Common.Exceptions;
using Xunit;

namespace XML.Content.Parser.Business.Tests.Validators
{

  [Category("XML Validation")]
  public class MandatoryXmlElementsTests : TestFoundation
  {
    [Theory]
    [InlineData("<test>hello</test>")]
    [InlineData("<TEST>hello</test>")]
    public void When_Passing_MessageContent_With_Proper_Xml_Element_Then_Validate_It_With_Should_Not_Throw_Exception(string messageContent)
    {
      //Arrange
      var mandatoryXmlElements = new List<string>
            {
                "<test>"
            };
      ValidationService.GetMandatoryXmlElements().Returns(mandatoryXmlElements);

      //Act
      var exception = Record.Exception(() => MandatoryXmlElementsValidator.Validate(messageContent));

      //Assert
      Assert.Null(exception);
    }

    [Theory]
    [InlineData("<test1><test2>hello</test2></test1>")]
    [InlineData("<test1>hello</test1><test2>hello</test2>")]
    public void When_Passing_MessageContent_With_Multiple_Proper_Xml_Element_Then_Validate_It_With_Should_Throw_Exception(string messageContent)
    {
      //Arrange
      var mandatoryXmlElements = new List<string>
            {
                "<test1>",
                "<test2>"
            };

      ValidationService.GetMandatoryXmlElements().Returns(mandatoryXmlElements);

      //Act
      var exception = Record.Exception(() => MandatoryXmlElementsValidator.Validate(messageContent));

      //Assert
      Assert.Null(exception);
    }

    [Theory]
    [InlineData("<test1>hello</test1>")]
    [InlineData("hello world")]
    public void When_Passing_MessageContent_With_Missing_Xml_Element_Then_Validate_It_With_Should_Throw_Exception(string messageContent)
    {
      //Arrange
      var mandatoryXmlElements = new List<string>
            {
                "<test1>",
                "<test2>"
            };
      ValidationService.GetMandatoryXmlElements().Returns(mandatoryXmlElements);

      AssertXmlContentParserExceptionIsThrown(messageContent, mandatoryXmlElements);
    }

    [Theory]
    [InlineData("<test></test>")]
    public void When_Passing_MessageContent_With_Non_Xml_Element_Then_Validate_It_With_Should_Throw_Exception(string messageContent)
    {
      //Arrange
      var mandatoryXmlElements = new List<string>
            {
                "<test>"
            };

      ValidationService.GetMandatoryXmlElements().Returns(mandatoryXmlElements);

      AssertXmlContentParserExceptionIsThrown(messageContent, mandatoryXmlElements);
    }

    private void AssertXmlContentParserExceptionIsThrown(string messageContent, List<string> mandatoryXmlElements)
    {
      //Act
      var exception = Assert.Throws<XmlContentParserException>(() =>
      {
        MandatoryXmlElementsValidator.Validate(messageContent);
      });

      //Assert
      Assert.Equal($"The specified message content does not contain all mandatory XML elements. Mandatory elements: '{string.Join(",", mandatoryXmlElements)}'.", exception.Message);
    }
  }
}

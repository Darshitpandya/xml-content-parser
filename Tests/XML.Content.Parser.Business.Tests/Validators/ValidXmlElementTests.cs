using System.ComponentModel;
using XML.Content.Parser.Business.Tests.Stubs.TestFoundations;
using XML.Content.Parser.Common.Exceptions;
using Xunit;

namespace XML.Content.Parser.Business.Tests.Validators
{
  [Category("XML Validation")]
  public class ValidXmlElementTests : TestFoundation
  {
    [Theory]
    [InlineData("<test>")]
    [InlineData("<TEST>")]
    [InlineData("<test_123>")]
    [InlineData("</test>")]
    [InlineData("</TEST>")]
    [InlineData("</test_123>")]
    public void When_Passing_MessageContent_Then_Validate_It_With_Should_Not_Throw_Exception(string xmlElement)
    {
      //Act
      var exception = Record.Exception(() => ValidXmlElementsValidator.Validate(xmlElement));

      //Assert
      Assert.Null(exception);
    }

    [Theory]
    [InlineData("hello world")]
    [InlineData("<test />")]
    [InlineData("<test")]
    [InlineData("test>")]
    [InlineData("<te st>")]
    [InlineData("</test")]
    [InlineData("/test>")]
    [InlineData("<//test>")]
    [InlineData("</te /st>")]
    [InlineData("<  test  >")]
    [InlineData("<  /  test  >")]
    public void When_Passing_MessageContent_Invalid_Xml_Element_Then_Validate_It_With_Should_Throw_Exception(string xmlElement)
    {
      //Act
      var exception = Assert.Throws<XmlContentParserException>(() =>
      {
        ValidXmlElementsValidator.Validate(xmlElement);
      });

      //Assert
      Assert.Equal("The specified message contains invalid XML syntax.", exception.Message);
    }
  }
}

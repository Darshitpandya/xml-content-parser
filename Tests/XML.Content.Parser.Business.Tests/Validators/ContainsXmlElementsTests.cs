using System.ComponentModel;
using XML.Content.Parser.Business.Tests.Stubs.TestFoundations;
using XML.Content.Parser.Common.Exceptions;
using Xunit;

namespace XML.Content.Parser.Business.Tests.Validators
{
  [Category("XML Validation")]
  public class ContainsXmlElementsTests : TestFoundation
  {
    [Theory]
    [InlineData("<test></test>")]
    [InlineData("<TEST></TEST>")]
    [InlineData("<test_element></test_element>")]
    [InlineData("<123></123>")]
    public void When_Passing_MessageContent_With_Xml_Then_Validate_It_With_And_Should_Not_Throw_Exception(string messageContent)
    {
      //Act
      var exception = Record.Exception(() => ContainsXmlElementsValidator.Validate(messageContent));

      //Assert
      Assert.Null(exception);
    }

    [Theory]
    [InlineData("hello world")]
    [InlineData("<bad xml></bad xml>")]
    [InlineData("<aB1@#></aB1@#>")]
    public void When_Passing_MessageContent_With_InvalidXml_Then_Validate_It_With_And_Should_Throw_Exception(string messageContent)
    {
      AssertXmlContentParserExceptionIsThrown(messageContent);
    }

    [Theory]
    [InlineData("<test><test>")]
    public void When_Passing_MessageContent_Without_Closing_Xml_Element_Then_Validate_It_With_Should_Throw_Exception(string messageContent)
    {
      AssertXmlContentParserExceptionIsThrown(messageContent);
    }

    [Theory]
    [InlineData("</test></test>")]
    public void When_Passing_MessageContent_Without_Opening_Xml_Element_Then_Validate_It_With_Should_Throw_Exception(string messageContent)
    {
      AssertXmlContentParserExceptionIsThrown(messageContent);
    }

    private void AssertXmlContentParserExceptionIsThrown(string messageContent)
    {
      //Act
      var exception = Assert.Throws<XmlContentParserException>(() =>
      {
        ContainsXmlElementsValidator.Validate(messageContent);
      });

      //Assert
      Assert.Equal("The specified message content does not contain any valid XML elements.", exception.Message);
    }
  }
}

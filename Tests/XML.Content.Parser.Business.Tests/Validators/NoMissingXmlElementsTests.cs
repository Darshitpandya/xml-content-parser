using System.ComponentModel;
using XML.Content.Parser.Business.Tests.Stubs.TestFoundations;
using XML.Content.Parser.Common.Exceptions;
using Xunit;

namespace XML.Content.Parser.Business.Tests.Validators
{

  [Category("XML Validation")]
  public class NoMissingXmlElementsTests : TestFoundation
  {
    [Theory]
    [InlineData("<test1></test1><test2></test2>")]
    [InlineData("<test></TEST>")]
    [InlineData("<parent><child></child></parent>")]
    public void When_Passing_MessageContent_With_Matching_Xml_Element_Then_Validate_It_With_Should_Not_Throw_Exception(string messageContent)
    {
      //Act
      var exception = Record.Exception(() => NoMissingXmlElementsValidator.Validate(messageContent));

      //Assert
      Assert.Null(exception);
    }

    [Theory]
    [InlineData("<test1></test2>")]
    public void When_Passing_MessageContent_With_MisMatching_Xml_Element_Then_Validate_It_With_Should_Throw_Exception(string messageContent)
    {
      AssertXmlContentParserExceptionIsThrown(messageContent);
    }

    [Theory]
    [InlineData("<test1></test1><test1></test2>")]
    public void When_Passing_MessageContent_With_Uneven_Matching_Xml_Element_Then_Validate_It_With_Should_Throw_Exception(string messageContent)
    {
      AssertXmlContentParserExceptionIsThrown(messageContent);
    }

    [Theory]
    [InlineData("<test1><test2>")]
    public void When_Passing_MessageContent_Without_Closing_Xml_Element_Then_Validate_It_With_Should_Throw_Exception(string messageContent)
    {
      AssertXmlContentParserExceptionIsThrown(messageContent);
    }

    [Theory]
    [InlineData("</test1></test2>")]
    public void When_Passing_MessageContent_Without_Opening_Xml_Element_Then_Validate_It_With_Should_Throw_Exception(string messageContent)
    {
      AssertXmlContentParserExceptionIsThrown(messageContent);
    }

    private void AssertXmlContentParserExceptionIsThrown(string messageContent)
    {
      //Act
      var exception = Assert.Throws<XmlContentParserException>(() =>
      {
        NoMissingXmlElementsValidator.Validate(messageContent);
      });

      //Assert
      Assert.Equal("The specified message content contains XML elements without it's corresponding pair.", exception.Message);
    }
  }
}

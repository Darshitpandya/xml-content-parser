using System.ComponentModel;
using XML.Content.Parser.Business.Contracts;
using XML.Content.Parser.Business.Mappers;
using XML.Content.Parser.Business.Tests.Stubs;
using Xunit;

namespace XML.Content.Parser.Business.Tests.Domain
{
  [Category("Domain")]
  public class ExpenseTests
  {
    [Fact]
    public void When_Contract_Is_Valid_Then_Calculate_Total_Should_Return_Expected_Total()
    {
      //Arrange
      var expenseContract = new ExpenseContract
      {
        Expense = new ExpenseDataContractBuilder()
          .WithCostCentre("DEV002")
          .WithTotal(1024.01m)
          .Build()
      };
      var vendorContract = new VendorContractBuilder().Build();
      var descriptionContract = new DescriptionContractBuilder().Build();
      var eventDateContract = new EventDateContractBuilder().Build();

      //Act
      var expense = ExpenseMapper.Map(expenseContract, vendorContract, descriptionContract, eventDateContract);

      //Assert
      Assert.Equal(expenseContract.Expense.CostCentre, expense.CostCentre);
      Assert.Equal(expenseContract.Expense.Total, expense.TotalInclGst);
      Assert.Equal(890.44m, expense.TotalExclGst);
      Assert.Equal(133.57m, expense.GstAmount);
      Assert.Equal(vendorContract.Vendor, expense.Vendor);
      Assert.Equal(descriptionContract.Description, expense.Description);
      Assert.Equal(eventDateContract.Date, expense.EventDate);
    }

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    [InlineData(" ")]
    public void When_Contract_Is_Missing_CostCentre_Then_Calculate_Should_Return_Unknown(string costCentre)
    {
      //Arrange
      var expenseContract = new ExpenseContract
      {
        Expense = new ExpenseDataContractBuilder()
          .WithCostCentre(costCentre)
          .WithTotal(1024.01m)
          .Build()
      };
      var vendorContract = new VendorContractBuilder().Build();
      var descriptionContract = new DescriptionContractBuilder().Build();
      var eventDateContract = new EventDateContractBuilder().Build();

      //Act
      var expense = ExpenseMapper.Map(expenseContract, vendorContract, descriptionContract, eventDateContract);

      //Assert
      Assert.Equal("UNKNOWN", expense.CostCentre);
    }
  }
}

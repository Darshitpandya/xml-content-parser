using System;
using Microsoft.AspNetCore.Hosting;
using NLog.Web;

namespace XML.Content.Parser.API.WebHost
{
  /// <summary>
  /// Represents the entry point of the application.
  /// </summary>
  public class Program
  {
    /// <summary/>
    /// <param name="args"></param>
    public static void Main(string[] args)
    {
      // NLog: setup the logger first to catch all errors.
      var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
      try
      {
        logger.Debug("Main entrypoint");
        BuildWebHost(args).Run();
      }
      catch (Exception ex)
      {
        //NLog: catch setup errors 
        logger.Error(ex, "Stopped program because of startup exception");
        throw;
      }
      finally
      {
        // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
        NLog.LogManager.Shutdown();
      }

    }

    /// <summary>
    /// Create web host holder
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static IWebHost BuildWebHost(string[] args) =>
        Microsoft.AspNetCore.WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>()
           .Build();
  }
}

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace XML.Content.Parser.API.Infrastructure
{
  /// <summary>
  /// API pipeline configuration
  /// </summary>
  public static class ApiPipelineConfiguration
  {
    /// <summary/>
    /// <param name="app"></param>
    /// <param name="configuration"></param>
    /// <param name="env"></param>
    public static void ConfigureCollaborationPipeline(this IApplicationBuilder app, IConfiguration configuration, IHostingEnvironment env)
    {
      //must be first to catch all excpetions in pipeline below
      app.UseMiddleware(typeof(ExceptionHandlingMiddleware));

      // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
      // specifying the Swagger JSON endpoint.
      app.UseSwagger();
      app.UseSwaggerUI(c =>
      {
        c.SwaggerEndpoint($"/swagger/v1/swagger.json", "XML.Content.Parser.API v1");
      });

      app.UseMvc();
    }
  }
}

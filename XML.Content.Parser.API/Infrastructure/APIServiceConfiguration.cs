using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;
using XML.Content.Parser.API.Swagger;
using XML.Content.Parser.Business.Factories;
using XML.Content.Parser.Business.Interfaces;
using XML.Content.Parser.Business.Services;

namespace XML.Content.Parser.API.Infrastructure
{
  /// <summary>
  /// API servcie configuration to build up DI container
  /// </summary>
  public static class ApiServiceConfiguration
  {
    /// <summary>
    /// This is called from application.StartUp and build up DI container
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuration"></param>
    public static void ConfigureCollaborationServices(this IServiceCollection services, IConfiguration configuration)
    {
      services
        .AddMvcCore()
        .AddApiExplorer()
        .AddDataAnnotations()
        .AddFormatterMappings()
        .AddJsonFormatters()
        .AddJsonOptions(options =>
          {
            options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
          }
        );

      services.AddSwaggerGen(c =>
      {
        c.SwaggerDoc("v1", new Info { Title = "XML Content Parser API", Version = "v1" });
        c.OperationFilter<DefaultHttpResponcesFilter>();

        // Set the comments path for the Swagger JSON and UI.
        var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
        c.IncludeXmlComments(xmlPath);
      });


      //Services
      services.AddTransient<IExpenseService, ExpenseService>();
      services.AddTransient<IIdentifyXmlElementsService, IdentifyXmlElementsService>();
      services.AddTransient<IValidationService, ValidationService>();
      services.AddTransient<IXmlDeserializerService, XmlDeserializerService>();

      //Factories
      services.AddTransient<IXmlValidationFactory, XmlValidationFactory>();
    }
  }
}

using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using XML.Content.Parser.Common.Exceptions;

namespace XML.Content.Parser.API.Infrastructure
{
  /// <summary>
  /// 
  /// </summary>
  public class ExceptionHandlingMiddleware
  {
    private readonly RequestDelegate _next;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="next"></param>
    public ExceptionHandlingMiddleware(RequestDelegate next)//do not di here, this is config only params
    {
      _next = next;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="logger"></param>
    /// <returns></returns>
    public async Task Invoke(HttpContext context, ILogger<ExceptionHandlingMiddleware> logger)//can di here
    {
      try
      {
        await _next(context);
      }
      catch (Exception ex)
      {
        await HandleExceptionAsync(context, ex, logger);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="exception"></param>
    /// <param name="logger"></param>
    /// <returns></returns>
    private static async Task HandleExceptionAsync(HttpContext context, Exception exception, ILogger logger)
    {
      var code = HttpStatusCode.InternalServerError;

      if(exception is XmlContentParserException) code = HttpStatusCode.BadRequest;

      //log as error only 500
      if (code == HttpStatusCode.InternalServerError)
      {
        logger.LogError(exception, "Unhandled exception detected and converted to 500 response.");
      }
      else
      {
        logger.LogInformation(exception, "Exception handled by middleware and converted to {0} response.", code);
      }

      context.Response.StatusCode = (int)code;

      if (!context.Response.HasStarted)
      {
        var result = JsonConvert.SerializeObject(new {error = exception.Message});
        context.Response.ContentType = "application/json";
        await context.Response.WriteAsync(result);
      }
    }
  }
}

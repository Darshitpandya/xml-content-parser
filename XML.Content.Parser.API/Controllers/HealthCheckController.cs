using Microsoft.AspNetCore.Mvc;

namespace XML.Content.Parser.API.Controllers
{
  /// <inheritdoc />
  [ApiExplorerSettings(IgnoreApi = true)]
  [Route("healthcheck")]
  public class HealthCheckController : ControllerBase
  {
    /// <summary>
    /// This controller for quick health check.
    /// </summary>
    /// <returns></returns>
    [Route("")]
    [HttpGet]
    public IActionResult GetHealth()
    {
      return Ok("All Good :) ");
    }
  }
}

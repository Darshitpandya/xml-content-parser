using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using XML.Content.Parser.Business.Interfaces;
using System;
using ExpenseContract = XML.Content.Parser.API.Contracts.ExpenseContract;

namespace XML.Content.Parser.API.Controllers.API
{
  /// <inheritdoc />
  /// <summary>
  /// </summary>
  [Route("api/expense")]
  [ApiController]
  public class ExpenseController : ControllerBase
  {

    private readonly IExpenseService _expenseService;

    /// <summary>
    /// Constructor for ExpenseController
    /// </summary>
    /// <param name="expenseService"></param>
    public ExpenseController(IExpenseService expenseService)
    {
      _expenseService = expenseService ?? throw new ArgumentNullException(nameof(expenseService));
    }

    /// <summary>
    /// Extract the XML content from the requested message from body.
    /// </summary>
    /// <param name="messageContent"></param>
    /// <returns></returns>
    [HttpPost("extract")]
    public ActionResult<ExpenseContract> ExtractExpense([FromBody] [Required] string messageContent)
    {
        var expense = _expenseService.Extract(messageContent);
        return Ok(expense);
    }
  }
}

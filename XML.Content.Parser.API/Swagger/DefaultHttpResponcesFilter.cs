using System.Collections.Generic;
using System.Net;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace XML.Content.Parser.API.Swagger
{
  /// <summary>
  /// Swagger Response filters
  /// </summary>
  public class DefaultHttpResponcesFilter : IOperationFilter
  {
    private readonly HttpStatusCode[] _defaultCodes = {
      HttpStatusCode.BadRequest,
      HttpStatusCode.InternalServerError
    };
    /// <summary/>
    /// <param name="operation"></param>
    /// <param name="context"></param>
    public void Apply(Operation operation, OperationFilterContext context)
    {
      if (operation.Responses == null)
        operation.Responses = new Dictionary<string, Response>();
      foreach (var code in _defaultCodes)
      {
        var codeNum = ((int)code).ToString();
        var codeName = code.ToString();
        if (!operation.Responses.ContainsKey(codeNum))
          operation.Responses.Add(codeNum, new Response { Description = codeName });
      }
    }
  }
}
